<div class="bx_footer row">
  <div class="bx_footer_top">
    <h2 class="logo_ft"><a href="#"><img src="./common/images/logo_ft.png" alt="logo footer"></a></h2>
    <p>
      販売元：株式会社アリオス<br>
      〒162-0067　東京都新宿区富久町7－8<br>サンクタス市ヶ谷富久町ウェストテラス1F<br>
    </p>
    <p>tel <a class="tel" href="tel:03-6380-5221">03-6380-5221</a>　 /　fax 03-6380-5222</p>
  </div>
  <!--/.top-->
</div>
<p class="copyright">Copyright &copy; 2018 Alios Co.,Ltd. All Rights Reserved.</p>