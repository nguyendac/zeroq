<section class="sec3">
	<div class="row">
		<h3><img src="./images/sec3_tt.png" alt="Tittle"></h3>
		<div class="sec3_step">
			<div class="sec3_inner">
				<dl>
					<dt>フローティング <br>スクリーンシステム<span>（FSS）</span></dt>
					<dd>
						<figure class="effect fadeIn delay_06">
					    <img src="./images/sec3_img1.png" alt="Images 01">
					  </figure>
						<p>複合痩身機としてはトップクラスであるのはもちろん操作パネルへのタッチレス機能を装備！SF映画の中でしか見ることの無かった実態のない操作パネルを空間に表現させると言う難題をリアル世界で見事に具現化しています！！<br><br>FSSを搭載した業務用美容機としてはエアーゼルクが世界初であることからこの機械最大の特徴と言ってもよいでしょう！この機能によりタッチパネルの汚れや画面の隙間からジェルやオイルが入る事により起こりえる故障のリスクをゼロにすることに成功しています。</p>
					</dd>
				</dl>
			</div>
			<div class="sec3_inner">
				<dl>
					<dt>吸引機能<br>ウルトラパルス搭載<span>（UVC）</span></dt>
					<dd>
						<figure class="effect fadeIn delay_06">
					      <img src="./images/sec3_img2.png" alt="Images 02">
					  </figure>
						<p>吸引を持続させるシングルパルスから1秒間に15回の吸引を実現するウルトラパルスまでなんと14種もの吸引パターンを持ち合わせています。お客様に合わせた吸引設定で他機種では真似のできない心地よい施術なのに大きな効果を期待できます。</p>
					</dd>
				</dl>
			</div>
			<div class="sec3_inner">
				<dl>
					<dt>ラジオ波<span>（RF）</span></dt>
					<dd>
						<figure class="effect fadeIn delay_06">
				      <img src="./images/sec3_img3.png" alt="Images 03">
				    </figure>
						<p>マルチポーラタイプを採用することにより効率よく深部を温めことが出来ます。ラジオ波の熱により脂肪層が柔らかく解れ毛細血管の血流を良くします。さらにはパーツ用プローブを使うことにより二重あご・足首・二の腕など繊細な動作が必要な箇所も容易に施術を行えます。</p>
					</dd>
				</dl>
			</div>
			<div class="sec3_inner">
				<dl>
					<dt>キャビテーション<span>（CAV）</span></dt>
					<dd>
						<figure class="effect fadeIn delay_06">
				      <img src="./images/sec3_img4.png" alt="Images 04">
				    </figure>
						<p>オートMODE搭載によって気になる骨伝導音（ノイズ）を軽減15分～20分の短い施術でしっかりした結果を期待でき強固なセルライトも短時間で柔らかくしていきます。</p>
					</dd>
				</dl>
			</div>
			<div class="sec3_inner">
				<dl>
					<dt>フェイシャル専用プローブ<span>（FC）</span></dt>
					<dd>
						<figure class="effect fadeIn delay_06">
				      <img src="./images/sec3_img5.png" alt="Images 05">
				    </figure>
						<p>目元周りのケアを筆頭に唇やほうれい線のケアまでお顔のケアには欠かせない専用プローブも標準装備となっています。さらにLEDの相乗効果で理想のフェイスラインをメイクします。</p>
					</dd>
				</dl>
			</div>
		</div>
		<div class="sec3_video">
			<h4>ムービーで確認！<br>エアーゼルクの圧倒的魅力</h4>
			<iframe src="https://www.youtube.com/embed/4dxOWUTj6tM" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>
</section>
<section class="sec4">
	<div class="row">
		<h3>AIR ZERUQ導入で</h3>
		<figure>
	    <img src="./images/sec4_tt.png" alt="Title">
	  </figure>
		<p class="sec4_subtt">しっかりとしたフォロー体制で、<br>売り上げアップサロン続出! </p>
		<div class="sec4_ct">
			<div class="sec4_ct_inner">
				<ul>
					<li>メニューはどうしたらいい?</li>
					<li>初めてできちんと使いこなせる不安...</li>
					<li>最新のサロン業界情報を知りたい !</li>
				</ul>
				<p>導入〜アフターフォローまでしっかりサポート! まとまった出費を避けたい方、<span>リース・ ビジネスクレジット・ レンタル</span>など支払い方法についてもお気軽にご相談ください。</p>
			</div>
		</div>
	</div>
</section>
<section class="sec5">
	<div class="row">
		<h3>アリオスができること</h3>
		<div class="sec5_inner">
			<h4>サロン経営・開業<br>トータルサポート！</h4>
			<div class="sec5_inner_ct">
				<figure class="effect fadeIn delay_06">
					<figcaption>店舗レイアウト</figcaption>	
					<img src="./images/img1.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>店舗ロゴ制作</figcaption>	
					<img src="./images/img2.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>価格設定相談</figcaption>	
					<img src="./images/img3.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>最新広告方法</figcaption>	
					<img src="./images/img4.png" alt="Title">
				</figure>

				<figure class="effect fadeIn delay_06">
					<figcaption>店舗チラシ作成</figcaption>	
					<img src="./images/img5.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>カウンセリングツール</figcaption>	
					<img src="./images/img6.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>誓約書ツール作成</figcaption>	
					<img src="./images/img7.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>店舗内印刷物作成</figcaption>	
					<img src="./images/img8.png" alt="Title">
				</figure>

				<figure class="effect fadeIn delay_06">
					<figcaption>ドクターサポート</figcaption>	
					<img src="./images/img9.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>商圏データレポート</figcaption>	
					<img src="./images/img10.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>ホームページ制作</figcaption>	
					<img src="./images/img11.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>集客方法マニュアル提供</figcaption>	
					<img src="./images/img12.png" alt="Title">
				</figure>

				<figure class="effect fadeIn delay_06">
					<figcaption>オリジナルアプリ提供</figcaption>	
					<img src="./images/img13.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>自動メール配信サポート</figcaption>	
					<img src="./images/img14.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>融資相談</figcaption>	
					<img src="./images/img15.png" alt="Title">
				</figure>
				<figure class="effect fadeIn delay_06">
					<figcaption>事業計画書作成</figcaption>	
					<img src="./images/img16.png" alt="Title">
				</figure>
			</div>
		</div>
	</div>
</section>
<section class="sec6 effect fadeIn delay_06">
	<div class="row">
		<h3><img src="./images/sec6_tt.png" alt="Title"></h3>
		<h4><img src="./images/sec6_subtt.png" alt="Title"></h4>
		<div class="sec6_inner">
			<div class="sec6_inner_ct">
				<em>保証内容</em>
				<div class="sec6_inner_ct_t">
					<p><span>総合保険</span></p>
					<p><span>医療提供</span></p>
					<p><img src="./images/sec6_ct.png" alt="Title"></p>
				</div>
			</div>
			<figure>
				<figcaption>事業計画書作成</figcaption>	
				<img src="./images/sec5_img.png" alt="Title">
			</figure>
		</div>
	</div>
</section>
<section class="contact">
	<div class="row">
		<div class="contact_inner">
			<h3>お問い合わせ・お申込みはこちら</h3>
			<div class="contact_inner_list">
				<a class="phone" href="tel:0120135130"><img src="./images/phone.png" alt="0120-135-130"></a>
			</div>
			<div class="btn">
        <a href="#"><span>今すぐメールでお問い合わせ</span></a>
      </div>
		</div>
	</div>
</section>