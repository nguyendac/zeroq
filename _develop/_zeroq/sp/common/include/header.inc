<div class="bx_header row">
  <h1 class="bx_header_logo">
    <a href=""><img src="/common/images/logo.png" alt="Logo"></a>
  </h1>
  <!--/.logo-->
  <div class="bx_header_contact">
    <div class="bx_header_contact_l">
      <div class="gr_tel">
        <a href="tel:0120-135-130">0120_135_130</a>
        <em>受付：10:00〜19:00(土日・祝日を除く)</em>
      </div>
      <!--/.gr_tel-->
    </div>
    <!--/left-->
    <div class="bx_header_contact_r">
      <div class="btn">
        <a href="#"><span>お問い合わせ</span></a>
      </div>
      <!--/.btn-->
    </div>
    <!--/.right-->
  </div>
  <!--/.contact-->
</div>