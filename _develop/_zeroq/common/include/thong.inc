<section class="st_banner">
  <div class="bx_banner row">
    <p class="txt_top">進化との遭遇、かつてないステージへ。</p>
    <span class="txt_fe">FES　搭載複合痩身機</span>
    <figure>
      <img src="./images/logo_lg.png" alt="logo">
    </figure>
    <h2 class="ttl_banner"><img src="./images/ttl_bn.png" alt="Title Banner"></h2>
    <div class="bx_fss">
      <div class="bk_b">
        <h3><img src="./images/ttl_sm.png" alt="Title small"></h3>
        <p>タッチパネルと違い、空中に操作画面があるため、ジェルやオイルがついた手での操作による、画面汚れや、それに伴うマシンの故障はなくなります。</p>
        <em>※反射物など何もない空間に操作画面を表示させる新技術。</em>
        <span class="icon_sys"><img src="./images/lg_system.png" alt="Logo system"></span>
      </div>
    </div>
    <!--/.bx_fss-->
    <em>※FSS搭載に関するご相談、又はOEM機などのご相談は株式会社アリオスにお問い合わせください。</em>
  </div>
</section>
<!--/.st_banner-->
<section class="st_encounter effect">
  <div class="bx_ent row">
    <h2 class="ttl_ent">ENCOUNTER</h2>
    <span class="ttl_sub">New technology</span>
    <div class="gr_art">
      <article class="effect delay_03">
        <h3><img src="./images/ttl_c_01.png" alt="ttk 01"></h3>
        <span><img src="./images/sp_c_01.png" alt="Span 01"></span>
        <p>40KHzのキャビテーションが、<br>脂肪層の適切な深さに届き、<br>効果を増大。</p>
      </article>
      <article class="effect delay_03">
        <h3><img src="./images/ttl_c_02.png" alt="ttk 02"></h3>
        <span><img src="./images/sp_c_02.png" alt="Span 02"></span>
        <p>温熱が心地良さをもたらし<br>ながら、脂肪を燃焼・<br>代謝を上げる。</p>
      </article>
      <article class="effect delay_06">
        <h3><img src="./images/ttl_c_03.png" alt="ttk 03"></h3>
        <span><img src="./images/sp_c_03.png" alt="Span 03"></span>
        <p>ソフトなボディケアから<br>頑固なセルライトまで、<br>オーダーメイド施術が可能。</p>
      </article>
      <article class="effect delay_06">
        <h3><img src="./images/ttl_c_04.png" alt="ttk 04"></h3>
        <span><img src="./images/sp_c_04.png" alt="Span 04"></span>
        <p>光が肌トラブルの<br>改善を促す。</p>
      </article>
    </div>
    <!--/.gr_art-->
  </div>
</section>
<!--/.st_encounter-->
<section class="st_type_hand">
  <div class="row">
    <h2 class="ttl_type">ハンドピースの種類</h2>
    <div class="gr_type">
      <article class="effect fadeIn delay_03">
        <figure>
          <img src="./images/pro_01.png" alt="Product 01">
        </figure>
        <h3>ボディ用キャビテーション</h3>
        <div class="main_art">
          <ul class="list_gt">
            <li>超音波振動により脂肪層に空洞化現象が起こり気泡をつくりだし、その気泡の消滅時に発せられる衝撃圧が脂肪細胞壁を共振させ脂肪減少効果が期待できる</li>
            <li>40KHzのキャビテーション波長域は最も安全に効果が期待できる領域波長</li>
          </ul>
        </div>
        <!--/.main_art-->
      </article>
      <article class="effect fadeIn delay_03">
        <figure>
          <img src="./images/pro_02.png" alt="Product 02">
        </figure>
        <h3>ボディ用(大)　RF＋吸引＋赤LED</h3>
        <div class="main_art">
          <ul class="list_gt">
            <li>温熱効果により細胞内酸素拡散を調整</li>
            <li>マイクロサーキュレーション効果（微笑循環）　※1</li>
            <li>脂肪細胞の代謝促進</li>
            <li>脂質分解の活性 </li>
            <li>肌質の弾力性向上</li>
          </ul>
          <em>※血液中の老廃物を除去し毛細血管までサラサラに流れることによりあらゆる身体の予防に繋がります。</em>
        </div>
        <!--/.main_art-->
      </article>
      <article class="effect fadeIn delay_06">
        <figure>
          <img src="./images/pro_03.png" alt="Product 03">
        </figure>
        <h3>ボディ用（中）RF＋吸引＋赤LED</h3>
        <div class="main_art">
          <ul class="list_gt">
            <li>二の腕や腿回りなどの繊細なケアが可能</li>
            <li>１４種もの吸引パターンによる振動効果</li>
            <li>高速パルスを実現するウルトラパルス機能搭載</li>
            <li>コラーゲン繊維のケア</li>
            <li>最新LED光ケア＋マルチポーラRF</li>
          </ul>
        </div>
        <!--/.main_art-->
      </article>
      <article class="effect fadeIn delay_06">
        <figure>
          <img src="./images/pro_04.png" alt="Product 04">
        </figure>
        <h3>フェイス用（小）RF＋吸引＋青LED</h3>
        <div class="main_art">
          <ul class="list_gt">
            <li>スキンファーミング効果　※２</li>
            <li>独自の吸引システムによるバイブレーション機能</li>
            <li>目元ケアにも最適なハンドピースデザイン</li>
          </ul>
          <em>※２　肌を栄養分の受け入れやすい健康的な土壌に改善</em>
        </div>
        <!--/.main_art-->
      </article>
    </div>
    <!--/.gr_type-->
    <div class="bx_specific effect fadeIn delay_06">
      <h3 class="ttl_spec">基本スペック</h3>
      <div class="spec_infor">
        <dl>
          <dt>キャビテーション</dt>
          <dd>40KHz±10％</dd>
        </dl>
        <dl>
          <dt>RF</dt>
          <dd>1.2MHz (Bi-Polar, Multi-Polar)</dd>
        </dl>
        <dl>
          <dt>吸引圧</dt>
          <dd>350mmHg</dd>
        </dl>
        <dl>
          <dt>LED</dt>
          <dd>Red LED (630nm), Blue LED (430nm)</dd>
        </dl>
        <dl>
          <dt>本体サイズ</dt>
          <dd>W50×D56.5×H103.5cm (最大154cm)</dd>
        </dl>
        <dl>
          <dt>本体重量</dt>
          <dd>kg</dd>
        </dl>
        <dl>
          <dt>消費電力</dt>
          <dd>900W</dd>
        </dl>
        <dl>
          <dt>電源・電圧</dt>
          <dd>220V / 110V, 50-60Hz</dd>
        </dl>
      </div>
      <!--/.spec_infor-->
    </div>
    <!--/.bx_specific-->
  </div>
</section>
<!--/.st_type_hand-->
<section class="st_face_body">
  <div class="bx_f_body row">
    <h2 class="ttl_face"><span><ins>フ</ins>ェイス</span><span><ins>ボ</ins>ディ複合機</span></h2>
    <h3><img src="./images/ttl_sub_face.png" alt="Title sub face body"></h3>
    <p>
      吸引マシンの多くは痛みを伴うものが多く<br>
      <span class="txt_g">施術自体を苦痛に感じる</span>お客様も少なくありませんでした。 <br>
      新型機エアーゼルクでは RFとバキューム(温めながらの吸引)の同時施術を実現することにより<br>
      従来のマシンより痛みを和らげお客様には<span class="txt_g">施術中もリラックスしていただけます。</span><br>
      吸引システムにはウルトラパルスを採用し吸引時に発生する運動量を大きくすることにも<br>
      成功しています。
    </p>
    <div class="bx_small">
      <small>10×20cm</small>
      <em>500Shot</em>
    </div>
    <!--/.bx_small-->
    <div class="bx_large">
      <em>脂肪層に<br>超音波を照射</em>
    </div>
    <!--/.bx_large-->
  </div>
  <ul class="list_img">
    <li><img src="./images/sample.png" alt="sample 01"></li>
    <li><img src="./images/sample_02.png" alt="sample 02"></li>
    <li><img src="./images/sample.png" alt="sample 03"></li>
  </ul>
</section>